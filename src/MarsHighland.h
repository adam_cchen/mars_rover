//
// Created by 陈成 on 2020/1/6.
//

#ifndef MARSROVER_MARSHIGHLAND_H
#define MARSROVER_MARSHIGHLAND_H


class MarsHighland {
public:
    MarsHighland(int width, int height);

    int
    getWidth() const;

    int
    getHeight() const;

private:
    int d_width;
    int d_height;
};


#endif //MARSROVER_MARSHIGHLAND_H
