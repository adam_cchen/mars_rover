//
// Created by 陈成 on 2020/1/6.
//

#ifndef MARSROVER_MARSROVER_H
#define MARSROVER_MARSROVER_H

#include <iostream>
#include "MarsHighland.h"

enum Direction {
    EAST, SOUTH, WEST, NORTH
};

enum Side {
    LEFT = -1,
    RIGHT = 1
};

class MarsRover {
public:
    MarsRover(int x, int y, Direction direction);

    int
    getX() const;

    int
    getY() const;

    int
    getDirection() const;

    void
    setCommands(const std::string &commands);

    int
    explore(MarsHighland *mh);

private:
    void
    rotate(Side side);

    void
    move();

private:
    int d_x;
    int d_y;
    Direction d_direction;
    std::string d_commands;
};


#endif //MARSROVER_MARSROVER_H
