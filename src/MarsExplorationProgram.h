//
// Created by 陈成 on 2020/1/6.
//

#ifndef MARSROVER_MARSEXPLORATIONPROGRAM_H
#define MARSROVER_MARSEXPLORATIONPROGRAM_H

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "MarsHighland.h"
#include "MarsRover.h"

class MarsExplorationProgram {
public:
    MarsExplorationProgram();
    ~MarsExplorationProgram();

    int
    initWithFile(std::string input_filename);

    int
    start();

    MarsHighland *
    getMarsHighland() const;

    std::vector<MarsRover> &
    getMarsRovers();

public:
    const static std::map<char, Direction> direction_mapping;

private:
    MarsHighland *d_mars_highland_p{nullptr};
    std::vector<MarsRover> d_mars_rovers;
};


#endif //MARSROVER_MARSEXPLORATIONPROGRAM_H
