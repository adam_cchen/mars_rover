//
// Created by 陈成 on 2020/1/6.
//

#include "MarsHighland.h"

int MarsHighland::getWidth() const {
    return d_width;
}

int MarsHighland::getHeight() const {
    return d_height;
}

MarsHighland::MarsHighland(int width, int height) {
    d_width = width;
    d_height = height;
}
