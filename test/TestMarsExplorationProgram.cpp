//
// Created by 陈成 on 2020/1/6.
//

#include "gtest/gtest.h"
#include "MarsExplorationProgram.h"

TEST(TestMarsExplorationProgram, input_file_should_exists) {
    MarsExplorationProgram mep{};

    EXPECT_EQ(mep.initWithFile("/Users/chencheng/CLionProjects/MarsRover/input.txt"), EXIT_SUCCESS);
}

TEST(TestMarsExplorationProgram, init_mars_highland_with_size) {
    MarsExplorationProgram mep{};

    EXPECT_EQ(mep.initWithFile("/Users/chencheng/CLionProjects/MarsRover/input.txt"), EXIT_SUCCESS);

    EXPECT_NE(mep.getMarsHighland(), nullptr);

    EXPECT_EQ(mep.getMarsHighland()->getWidth(), 5);
    EXPECT_EQ(mep.getMarsHighland()->getHeight(), 5);
}

TEST(TestMarsExplorationProgram, init_mars_rover_with_position_and_direction) {
    MarsExplorationProgram mep{};

    EXPECT_EQ(mep.initWithFile("/Users/chencheng/CLionProjects/MarsRover/input.txt"), EXIT_SUCCESS);

    EXPECT_FALSE(mep.getMarsRovers().empty());

    EXPECT_EQ(mep.getMarsRovers()[0].getX(), 1);
    EXPECT_EQ(mep.getMarsRovers()[0].getY(), 2);
    EXPECT_EQ(mep.getMarsRovers()[0].getDirection(), NORTH);
}

TEST(TestMarsExplorationProgram, mars_rovers_start_exploring) {
    MarsExplorationProgram mep{};

    EXPECT_EQ(mep.initWithFile("/Users/chencheng/CLionProjects/MarsRover/input.txt"), EXIT_SUCCESS);

    EXPECT_FALSE(mep.getMarsRovers().empty());
    
    EXPECT_EQ(mep.start(), EXIT_SUCCESS);

    EXPECT_EQ(mep.getMarsRovers()[0].getX(), 1);
    EXPECT_EQ(mep.getMarsRovers()[0].getY(), 3);
    EXPECT_EQ(mep.getMarsRovers()[0].getDirection(), NORTH);
}

