//
// Created by 陈成 on 2020/1/7.
//

#include <gtest/gtest.h>
#include "MarsRover.h"

TEST(TestMarsRover, facing_east_and_move_one_step) {
    MarsRover mr{0, 0, EAST};
    mr.setCommands("M");

    MarsHighland mh{5, 5};
    mr.explore(&mh);

    EXPECT_EQ(mr.getX(), 1);
    EXPECT_EQ(mr.getY(), 0);
    EXPECT_EQ(mr.getDirection(), EAST);
}

TEST(TestMarsRover, facing_south_and_move_one_step) {
    MarsRover mr{0, 1, SOUTH};
    mr.setCommands("M");

    MarsHighland mh{5, 5};
    mr.explore(&mh);

    EXPECT_EQ(mr.getX(), 0);
    EXPECT_EQ(mr.getY(), 0);
    EXPECT_EQ(mr.getDirection(), SOUTH);
}

TEST(TestMarsRover, facing_west_and_move_one_step) {
    MarsRover mr{1, 0, WEST};
    mr.setCommands("M");

    MarsHighland mh{5, 5};
    mr.explore(&mh);

    EXPECT_EQ(mr.getX(), 0);
    EXPECT_EQ(mr.getY(), 0);
    EXPECT_EQ(mr.getDirection(), WEST);
}

TEST(TestMarsRover, facing_north_and_move_one_step) {
    MarsRover mr{0, 0, NORTH};
    mr.setCommands("M");

    MarsHighland mh{5, 5};
    mr.explore(&mh);

    EXPECT_EQ(mr.getX(), 0);
    EXPECT_EQ(mr.getY(), 1);
    EXPECT_EQ(mr.getDirection(), NORTH);
}

TEST(TestMarsRover, facing_east_and_turn_left) {
    MarsRover mr{0, 0, EAST};
    mr.setCommands("L");

    MarsHighland mh{5, 5};
    mr.explore(&mh);

    EXPECT_EQ(mr.getX(), 0);
    EXPECT_EQ(mr.getY(), 0);
    EXPECT_EQ(mr.getDirection(), NORTH);
}

TEST(TestMarsRover, facing_east_and_turn_right) {
    MarsRover mr{0, 0, EAST};
    mr.setCommands("R");

    MarsHighland mh{5, 5};
    mr.explore(&mh);

    EXPECT_EQ(mr.getX(), 0);
    EXPECT_EQ(mr.getY(), 0);
    EXPECT_EQ(mr.getDirection(), SOUTH);
}

TEST(TestMarsRover, facing_south_and_turn_left) {
    MarsRover mr{0, 0, SOUTH};
    mr.setCommands("L");

    MarsHighland mh{5, 5};
    mr.explore(&mh);

    EXPECT_EQ(mr.getX(), 0);
    EXPECT_EQ(mr.getY(), 0);
    EXPECT_EQ(mr.getDirection(), EAST);
}

TEST(TestMarsRover, facing_south_and_turn_right) {
    MarsRover mr{0, 0, SOUTH};
    mr.setCommands("R");

    MarsHighland mh{5, 5};
    mr.explore(&mh);

    EXPECT_EQ(mr.getX(), 0);
    EXPECT_EQ(mr.getY(), 0);
    EXPECT_EQ(mr.getDirection(), WEST);
}

TEST(TestMarsRover, facing_west_and_turn_left) {
    MarsRover mr{0, 0, WEST};
    mr.setCommands("L");

    MarsHighland mh{5, 5};
    mr.explore(&mh);

    EXPECT_EQ(mr.getX(), 0);
    EXPECT_EQ(mr.getY(), 0);
    EXPECT_EQ(mr.getDirection(), SOUTH);
}

TEST(TestMarsRover, facing_west_and_turn_right) {
    MarsRover mr{0, 0, WEST};
    mr.setCommands("R");

    MarsHighland mh{5, 5};
    mr.explore(&mh);

    EXPECT_EQ(mr.getX(), 0);
    EXPECT_EQ(mr.getY(), 0);
    EXPECT_EQ(mr.getDirection(), NORTH);
}

TEST(TestMarsRover, facing_north_and_turn_left) {
    MarsRover mr{0, 0, NORTH};
    mr.setCommands("L");

    MarsHighland mh{5, 5};
    mr.explore(&mh);

    EXPECT_EQ(mr.getX(), 0);
    EXPECT_EQ(mr.getY(), 0);
    EXPECT_EQ(mr.getDirection(), WEST);
}

TEST(TestMarsRover, facing_north_and_turn_right) {
    MarsRover mr{0, 0, NORTH};
    mr.setCommands("R");

    MarsHighland mh{5, 5};
    mr.explore(&mh);

    EXPECT_EQ(mr.getX(), 0);
    EXPECT_EQ(mr.getY(), 0);
    EXPECT_EQ(mr.getDirection(), EAST);
}

TEST(TestMarsRover, move_out_of_highland_left_side) {
    MarsRover mr{0, 0, WEST};
    mr.setCommands("M");

    MarsHighland mh{5, 5};
    EXPECT_EQ(mr.explore(&mh), EXIT_FAILURE);
}

TEST(TestMarsRover, move_out_of_highland_right_side) {
    MarsRover mr{5, 0, EAST};
    mr.setCommands("M");

    MarsHighland mh{5, 5};
    EXPECT_EQ(mr.explore(&mh), EXIT_FAILURE);
}

TEST(TestMarsRover, move_out_of_highland_top_side) {
    MarsRover mr{0, 5, NORTH};
    mr.setCommands("M");

    MarsHighland mh{5, 5};
    EXPECT_EQ(mr.explore(&mh), EXIT_FAILURE);
}

TEST(TestMarsRover, move_out_of_highland_bottom_side) {
    MarsRover mr{0, 0, SOUTH};
    mr.setCommands("M");

    MarsHighland mh{5, 5};
    EXPECT_EQ(mr.explore(&mh), EXIT_FAILURE);
}
